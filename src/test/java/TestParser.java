import org.example.Element;
import org.example.Parser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestParser {

    @Test
    public void test1(){
        String content = "<html>content</html>";
        Parser parser = new Parser();
        parser.setContent(content);
        Element actual = parser.parse();
        Element expected = new Element();
        expected.setText("content");
        expected.setTag("html");
        Assertions.assertEquals(actual, expected);
    }
    @Test
    public void test2(){
        String content = "<html><h1>content1</h1><h2>content2</h2></html>";
        Parser parser = new Parser();
        parser.setContent(content);
        Element actual = parser.parse();
        Element expected = new Element();
        expected.setContent("<h1>content1</h1>\n<h2>content2</h2>");
        expected.setTag("html");
        Element anc1 = new Element();
        anc1.setParent(expected);
        anc1.setTag("h1");
        anc1.setText("content1");
        Element anc2 = new Element();
        anc2.setParent(expected);
        anc2.setTag("h2");
        anc2.setText("content2");
        List<Element> l = new ArrayList<>();
        l.add(anc1);
        l.add(anc2);
        expected.setAncestors(l);
        Assertions.assertEquals(actual, expected);
    }

    @Test
    public void linkedTest(){

    }

    @Test
    public void testP(){
        String content = "<html><a>hello</a><h1><h4>nestedHello</h4><h3>nestedWorld</h3><h6><br>top</br></h6></h1><br>world</br></html>";
        Parser parser = new Parser();
        parser.setContent(content.replace("\n", ""));
        Element actual = parser.parse();
        List<String> l = parser.fillStack(actual);
        l.forEach(System.out::println);
    }
}
