package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
    private String content;
    private String page;

    public Element parse() {
        page = content.replace("><", ">\n<");
        Element root = new Element();
        String line = page.split("\n")[0];
        String tag = determineTag(line);
        root.setTag(tag);
        root.setText(determineText(line));
        root.setContent(getInnerContent(this.page, tag));
        parseElements(root);
        return root;
    }

    public List<String> fillStack(Element element) {
        List<String> result = new ArrayList<>();
        if (element.isElementHasAncestors()) {
            result.add(element.getContent().replace("\n", ""));
            List<Element> ancestors = element.getAncestors();
            List<String> newList = new ArrayList<>();
            for (Element e :
                    ancestors) {
                newList.addAll(fillStack(e));
            }
            result.addAll(0, newList);
        } else {
            result.add(element.getText());
        }
            return result;
    }

    private void parseElements(Element parent) {
        Predicate<String> isElementHasText = Objects::nonNull;
        if (isElementHasText.test(parent.getText())) {
            //element has text, so it doesn't have ancestors - so it is last element in the hierarchy
            return;
        }
        String innerContent = parent.getContent();
        String[] lines = innerContent.split("\n");
        List<Element> ancestors = new ArrayList<>();

        for (int i = 0; i < lines.length; i++) {
            String tag = determineTag(lines[i]);
            String text = determineText(lines[i]);

            Element child = new Element();
            child.setTag(tag);
            child.setText(text);
            child.setParent(parent);
            ancestors.add(child);

            if (!isElementHasText.test(text)) {
                StringBuilder out = new StringBuilder();
                int endLine = getInnerContent(lines, i + 1, tag, out);
                child.setContent(out.toString().trim());
                parseElements(child);
                i = endLine;//all elements between lines[i] end lines[endLine] were parsed in the previous recursion call.
            }
        }
        parent.setAncestors(ancestors);
    }

    private int getInnerContent(String[] lines, int fromLine, String tag, StringBuilder out) {
        String endTag = "</" + tag + ">";
        for (int i = fromLine; i < lines.length; i++) {
            if (lines[i].equals(endTag)) {
                return i;
            }
            out.append(lines[i]).append("\n");
        }
        throw new RuntimeException(//todo: create custom exception
                String.format("End tag doesn't found in inner content. The searching tag: %s, inner content: %s",
                        endTag,
                        content));
    }

    //todo: concatenate getInnerContent(String content, String tag) and (String[] lines, int fromLine, String tag)
    private String getInnerContent(String content, String tag) {
        StringBuilder sb = new StringBuilder();
        String[] lines = content.split("\n");
        if (!lines[0].startsWith("<" + tag + ">")) {
            throw new RuntimeException(//todo: create custom exception
                    String.format("The content should be started from the tag <%s>.%n Full content: %s",
                            tag, content));
        }
        String endTag = "</" + tag + ">";
        if (lines[0].endsWith(endTag)) {
            //single line with text - no innerContent
            return null;
        }

        lines[0] = "";//skip the first tag
        for (String line :
                lines) {
            if (line.equals(endTag)) {//skip last tag
                return sb.toString().trim();
            }
            sb.append(line).append("\n");
        }

        throw new RuntimeException(//todo: create custom exception
                String.format("End tag doesn't found in inner content. The searching tag: %s, inner content: %s",
                        endTag,
                        content));
    }

    private String determineText(String input) {
        String regex = ">([^<]+)<";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    private String determineTag(String input) {
        String regex = "^<([^>]+)>";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            return matcher.group(1);
        }
        //todo: create custom exception
        throw new RuntimeException("Tag not found");
    }

    public String getAsHtmlPage() {
        return page;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
