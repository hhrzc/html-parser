package org.example;


import java.util.List;
import java.util.Objects;

public class Element {
    private String tag;
    private String text;
    private String content;
    private List<Element> ancestors;
    private Element parent;
    public boolean isElementHasAncestors(){
        return ancestors != null && !ancestors.isEmpty();
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getText() {
        return text;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Element> getAncestors() {
        return ancestors;
    }

    public void setAncestors(List<Element> ancestors) {
        this.ancestors = ancestors;
    }

    public Element getParent() {
        return parent;
    }

    public void setParent(Element parent) {
        this.parent = parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Element element = (Element) o;
        return Objects.equals(tag, element.tag) && Objects.equals(text, element.text) && Objects.equals(content, element.content) && Objects.equals(ancestors, element.ancestors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tag, text, content, ancestors);
    }

    @Override
    public String toString() {
        return "Element{" +
                "tag='" + tag + '\'' +
                ", text='" + text + '\'' +
                ", content='" + content + '\'' +
                ", child=" + ancestors +
                '}';
    }
}
